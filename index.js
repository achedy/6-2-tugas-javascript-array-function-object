// soal no 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var daftarhewan2 = daftarHewan.sort();

daftarhewan2.forEach((value, index) => {
   console.log(value);
});

// soal no 2
function introduce(data){
    var kata = 'Nama saya '+data.name+', umur saya '+data.age+' tahun, alamat saya di '+data.address+', dan saya punya hobby yaitu '+data.hobby+'!'
    return kata;
} 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) 

// soal no 3
function hitung_huruf_vokal(str){
    var count = str.match(/[aeiou]/gi).length;
    return count;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2

// soal no 4
function hitung(var_int){
    res =  -2 + (var_int * 2);
    return res
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8